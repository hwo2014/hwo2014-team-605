package hwo2014

import org.json4s._

object Protocol {
  implicit lazy val formats = DefaultFormats

  def parseMsg(msg: MsgWrapper) = msg match {
    case MsgWrapper("yourCar", _, data) => {
        StaticData.teamCar = Some(data.extract[CarID])
      }

    case MsgWrapper("gameInit", _, JObject(
        ("race", race) :: Nil
      )) => {
        StaticData.race = Some(race.extract[Race])
      }

    case MsgWrapper("gameStart", _, _) => {}

    case MsgWrapper("gameEnd", _, data) => {
        StaticData.gameEnd = Some(data.extract[GameEnd])
      }

    case MsgWrapper("tournamentEnd", _, _) => {}

    case MsgWrapper("turboAvailable", _, data) => {
        ChangingData.turboAvailable += 1
        ChangingData.turbo += data.extract[Turbo]
      }

    case MsgWrapper("turboStart", _, data) => {
        data.extract[CarID] match {
          case carId if carId.name == StaticData.teamCar.get.name => {
            ChangingData.turboActive = true
          }
          case _ =>
        }
      }

    case MsgWrapper("turboEnd", _, data) => {
        data.extract[CarID] match {
          case carId if carId.name == StaticData.teamCar.get.name => {
            ChangingData.turbo.dequeue
            ChangingData.turboActive = false
          }
          case _ =>
        }
      }

    case MsgWrapper("carPositions", _, JArray(carPositions)) => {
        carPositions map {
          carPosition => {
            carPosition.extract[CarPosition]
          }
        }
      }

    case MsgWrapper("lapFinished", Some(gameTick), data) => {
        data.extract[LapFinished]
      }

    case MsgWrapper("dnf", Some(gameTick), data) => {
        data.extract[Disqualification] match {
          case Disqualification(carId, reason) =>
            Debug.log(reason)
          case _ =>
        }
      }

    case MsgWrapper("crash", Some(gameTick), data) => {
        data.extract[CarID] match {
          case carId if carId.name == StaticData.teamCar.get.name => {
            Debug.log("Crashed!")
            ChangingData.crashed = true
          }
          case _ =>
        }
      }

    case MsgWrapper("spawn", Some(gameTick), data) => {
        data.extract[CarID] match {
          case carId if carId.name == StaticData.teamCar.get.name => {
            Debug.log("Spawned!")
            ChangingData.crashed = false
          }
          case _ =>
        }
      }

    case MsgWrapper("finish", Some(gameTick), data) => {
        data.extract[CarID]
      }

    case MsgWrapper(msgType, _, data) => {
      Debug.log("========")
      Debug.log("Received: " + msgType)
      Debug.log(" Contents: " + data.toString)
    }
  }
}
