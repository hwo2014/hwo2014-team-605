package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
import java.net.Socket
import java.io.{BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter}
import org.json4s.native.Serialization
import scala.annotation.tailrec

object ThatsMyBotting extends App {
  args.toList match {
    case hostName :: port :: botName :: botKey :: _ =>
      new ThatsMyBotting(hostName, Integer.parseInt(port), botName, botKey)
    case _ => println("args missing")
  }
}

class ThatsMyBotting(host: String, port: Int, botName: String, botKey: String) {
  implicit val formats = new DefaultFormats{}
  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))

  var throttleIndex = 0
  var throttles = List[(Double, Double, Double)]()
  var starting = true
  var lastPosition = 0.0
  var lastTick:Long = -1
  var switchSent = false

  send(MsgWrapper("join", None, Join(botName, botKey)))
  play

  @tailrec private def play {
    val line = reader.readLine()

    if (line != null) {
      val msg = Serialization.read[MsgWrapper](line)
      val tick = msg.gameTick.map(_.toInt).getOrElse(0)
      val parsed = Protocol.parseMsg(msg)
      if(msg.msgType == "gameInit") {
        throttles = Estimator.throttles(0, StaticData.race.get.raceSession.laps.getOrElse(1))
        throttleIndex = 0
        //Debug.log("tick,position,throttle,angle,velocity,targetVelocity")
      }

      def getThrottleIndex(currentIndex: Int, position: Double): Int = {
        for(i <- currentIndex until throttles.size) {
          val (_, endPosition, _) = throttles(i)
          if(position > endPosition) return i+1
        }
        return currentIndex
      }

      def recalculateThrottles(lane: Int, position: Double) {
        throttles = Estimator.throttles(lane, StaticData.race.get.raceSession.laps.getOrElse(1))
        throttleIndex = getThrottleIndex(0, position)
      }

      parsed match {
        case list: List[_] =>
          list map {
            case CarPosition(id, angle, piecePosition) if id == StaticData.teamCar.get =>
              val PiecePosition(index, inPiecePosition, LaneSwitch(lane, laneEnd), lap) = piecePosition
              val track = StaticData.race.get.track
              val position = track.distance(index.toInt, lane.toInt, lap) + inPiecePosition
              if(lane != laneEnd) {
                switchSent = false
                recalculateThrottles(laneEnd, position)
              }

              val newLane = lane.toInt

              val shouldTurbo = ChangingData.turboAvailable > 0
              if(shouldTurbo) {
                ChangingData.turboAvailable -= 1
                recalculateThrottles(lane.toInt, position)
              }

              val (oldThrottle, endPosition, _) = throttles(throttleIndex)
              val shouldSwitch = endPosition < position
              throttleIndex = getThrottleIndex(throttleIndex, position)

              val (prospectiveThrottle, _, targetVelocity) = throttles(throttleIndex)

              val velocity = position - lastPosition

              // while we've just started, speed up until we can get up to expected velocity
              if(starting) {
                val (_, endPosition, targetVelocity) = throttles(throttleIndex + 1)
                val expectedVel = Estimator.velocityAfter(velocity, endPosition - position, prospectiveThrottle)

                // give allowance
                if(expectedVel >= targetVelocity - 1.0) {
                  starting = false
                }
              }

              val newThrottle = if(starting) 1.0 else prospectiveThrottle

              //Debug.log(f"$tick,$position,$newThrottle%.16f,$angle%.16f,$velocity%.16f,$targetVelocity%.16f")

              lastPosition = position

              ChangingData.distance += piecePosition.traveled(
                ChangingData.lastPosition, track.pieces, track.lanes
              )
              ChangingData.lastPosition = Some(piecePosition)
              Estimator.feed(ChangingData.distance, tick, newThrottle)

              if(newThrottle != oldThrottle) {
                send(MsgWrapper("throttle", msg.gameTick, newThrottle))
              }
              else if(!switchSent && newLane != lane.toInt) {
                switchSent = true
                if(newLane < lane.toInt) {
                  send(MsgWrapper("switchLane", msg.gameTick, "Left"))
                }
                if(newLane > lane.toInt) {
                  send(MsgWrapper("switchLane", msg.gameTick, "Right"))
                }
              }
              else if(shouldTurbo) {
                send(MsgWrapper("turbo", msg.gameTick, "LEEROOOOOOY JENKIIIIIINS"))
              }
              else {
                // just in case it didn't get it last tick
                send(MsgWrapper("throttle", msg.gameTick, newThrottle))
              }
            case _ =>
          }

        case _ =>
      }
      play
    }
  }

  def send(msg: MsgWrapper) {
    msg.gameTick match {
      case Some(tick) if tick > lastTick => {
        sendActual(msg)
        lastTick = tick
      }
      case Some(tick) if tick <= lastTick => {
        Debug.log("Msg Discarded!")
        Debug.log("Expected tick greater than: " + lastTick.toString)
        Debug.log("Got: " + msg.toString)
      }
      case None => sendActual(msg)
    }
  }

  def sendActual(msg: MsgWrapper) {
    writer.println(Serialization.write(msg))
    writer.flush
  }
}

case class Join(name: String, key: String)
case class MsgWrapper(
  msgType: String, gameTick: Option[Long], data: JValue
) {}
object MsgWrapper {
  implicit val formats = new DefaultFormats{}

  def apply(
    msgType: String, gameTick: Option[Long] = None, data: Any = ""
  ): MsgWrapper = {
    MsgWrapper(msgType, gameTick, Extraction.decompose(data))
  }
}
