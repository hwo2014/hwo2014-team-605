package hwo2014

import scala.collection.mutable.Queue

case class Race(track: Track, cars: List[Car], raceSession: RaceSession)

case class Track(
  id: String, name: String,
  pieces: List[Piece], lanes: List[Lane],
  startingPoint: StartingPoint
) {
  def trackLength(laneIndex: Int) = pieces.foldLeft(0.0)(_+_.distance(lanes(laneIndex).distanceFromCenter))
  def distance(pieceIndex: Int, laneIndex: Int, laps: Int) =
    trackLength(laneIndex) * laps + pieces.take(pieceIndex).foldLeft(0.0)(_+_.distance(lanes(laneIndex).distanceFromCenter.toDouble))
}

case class Piece(
  length: Option[Double], switch: Option[Boolean],
  radius: Option[Double], angle: Option[Double]
) {
  def distance(centerOffset: Double) = this match {
    case Piece(Some(length), _, _, _) => length
    case Piece(_, _, Some(radius), Some(angle)) =>
      val realOffset = if(angle < 0) centerOffset else -centerOffset
      val realAngle = Math.toRadians(Math.abs(angle))
      (radius + realOffset) * realAngle
  }

  def safeVelocity(centerOffset: Double) = Math.min(Estimator.maxVelocity, this match {
    case Piece(Some(length), _, _, _) => Double.PositiveInfinity
    case Piece(_, _, Some(radius), Some(angle)) =>
      val realOffset = if(angle < 0) centerOffset else -centerOffset
      // just a temporary magic value
      Math.sqrt(radius + realOffset) / 1.6
  })
}

case class Lane(distanceFromCenter: Double, index:Long)

case class StartingPoint(position: Position, angle: Double)
case class Position(x: Double, y: Double)

case class Car(id: CarID, dimensions: Dimensions)
case class CarID(name: String, color: String)
case class Dimensions(length: Double, width: Double, guideFlagPosition: Double)

case class RaceSession(
  durationMs: Option[Long],
  laps: Option[Int], maxLapTimeMs: Option[Long], quickRace: Option[Boolean]
)

case class CarPosition(
  id: CarID, angle: Double, piecePosition: PiecePosition
)
case class PiecePosition(
  pieceIndex: Int, inPieceDistance: Double,
  lane: LaneSwitch, lap: Int
){
  def traveled(
    last: Option[PiecePosition], pieces: List[Piece], lanes: List[Lane]
  ) = last match {
    case Some(lastPosition) => {
      if(lastPosition.pieceIndex == pieceIndex){
        inPieceDistance - lastPosition.inPieceDistance
      } else {
        val lastLane = lanes(lastPosition.lane.endLaneIndex)
        val lastPiece = pieces(lastPosition.pieceIndex)
        val lastLength = lastPiece.distance(
          lastLane.distanceFromCenter.toDouble
        )
        val lastInPieceDistance = lastLength - lastPosition.inPieceDistance
        inPieceDistance + lastInPieceDistance
      }
    }
    case None => inPieceDistance
  }
}
case class LaneSwitch(startLaneIndex: Int, endLaneIndex: Int)

case class GameEnd(results: List[Result], bestLaps: List[Result])
case class Result(car: CarID, result: Record)
case class Record(
  laps: Option[Long], ticks: Option[Long], millis: Option[Long]
)

case class LapFinished(
  car: CarID, lapTime: Record, raceTime: Record, ranking: Ranking
)
case class Ranking(overall: Long, fastestLap: Long)

case class Disqualification(car: CarID, reason: String)

case class Turbo(
  turboDurationMilliseconds: Double,
  turboDurationTicks: Long,
  turboFactor: Double
)

object StaticData{
  var teamCar: Option[CarID] = None
  var race: Option[Race] = None
  var gameEnd: Option[GameEnd] = None
}

object ChangingData{
  var crashed: Boolean = false
  var turboAvailable: Int = 0
  var turboActive: Boolean = false
  var turbo: Queue[Turbo] = Queue[Turbo]()
  var lastPosition: Option[PiecePosition] = None
  var distance: Double = 0.0
}
