package hwo2014

class SlidingWindow(sz:Int) {

	val entries = new Array[Double](sz)
	
	private var currentIndex = 0
	private var currentSize = 0
	private var cachedAvg = 0.0
	private var cachedVar = 0.0
	private var dirty = true

	/** Adds a value to the sliding window. Fluent interface so you can chain it like e(1)(2)(3) to add [1, 2, 3]*/
	def apply(v:Double) : SlidingWindow = {
		entries(currentIndex) = v
		
		if ( currentIndex + 1 >= entries.size ) {
			currentIndex = 0
		} else {
			currentIndex += 1
		}

		if ( currentSize < entries.size ) {
			currentSize	+= 1
		}

		dirty = true
		
		return this
	}

	private def compute() : Unit = {
		//Reference: http://www.johndcook.com/standard_deviation.html
		val arr = entries.take(currentSize)
		val (cavg, cvar, _) = arr.drop(1).foldLeft( (arr.head, 0.0, 2) ) { case ((m, s, k), v) =>	
			val mm = m + (v - m)/k
			(mm, s + (v - m) * (v - mm), k+1)
		}
		cachedAvg = cavg
		cachedVar = cvar / (currentSize - 1)
		dirty = false
	}

	def avg = {
		if ( dirty ) {
			compute()
		}
		cachedAvg
	}

	def std = {
		if ( dirty ) {
			compute()
		}
		Math.sqrt(cachedVar)
	}

	def variance = {
		if ( dirty ) {
			compute()
		}
		cachedVar
	}

	def windowSize = entries.size
	def size = currentSize

	def isStable(threshold:Double) = (currentSize >= windowSize && variance <= threshold * threshold)

	def reset() : SlidingWindow = {
		currentIndex = 0
		currentSize = 0
		cachedAvg = 0.0
		cachedVar = 0.0
		dirty = true
		return this
	}
}


class StabilityDetector(windowSize:Int = 10, val threshold:Double = 0.1) {
	private val window = new SlidingWindow(windowSize)
	private var _ticks = 0

	def isStable = window.isStable(threshold)
	def get =
		if ( isStable ) {
			Some((window.avg, _ticks - window.size))
		} else {
			None
		}

	/** Adds a value to the detector. If a stable signal is detected, returns Some((signalLevel, ticks))
		where ticks is the number of values added to reach stability. */
	def apply(v:Double) = {
		val wasStable = isStable
		window(v)
		if ( isStable ) {
			if ( !wasStable ) {
				_ticks += 1
			}
			Some((window.avg, _ticks - window.size))
		} else {
			if ( wasStable ) {
				_ticks = 1
			} else {
				_ticks += 1
			}
			None
		}
	}
}