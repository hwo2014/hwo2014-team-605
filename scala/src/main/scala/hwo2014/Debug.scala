package hwo2014

object Debug{
  val on = false // turn off before commit
  def log(msg: String) {
    if(on) println(msg)
  }
}