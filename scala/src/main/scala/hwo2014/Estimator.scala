package hwo2014

import scala.collection.mutable.Queue

case class TickRecord(
  p: Double, t: Long, r: Double,
  v: Option[Double] = None, a: Option[Double] = None
)

case class Unknowns(engine: Double, drag: Double)

class FiniteQueue(size: Int, limit: Int) extends Queue[TickRecord]{
  var count = 0
  def isFull = (count > limit)
  override def enqueue(elems: TickRecord*) = {
    if(!isFull){
      val overflow = (this.length + elems.length) - size
      if(overflow > 0){
        1 to overflow foreach { _ => dequeue }
      }
      count += elems.length
      super.enqueue(elems:_*)
    }
  }
}

class Records extends FiniteQueue(size = 100, limit = 120){
  var unknowns = Unknowns(0, 0)
  def makeRecord(
    distance: Double,
    time: Long,
    throttle: Double
  ) = {
    val lastRecord = takeRight(1).fold(None)(
      (None, record) => Some(record)
    )

    val currentRecord = lastRecord match {
      case None => TickRecord(distance, time, throttle)
      case Some(TickRecord(p, t, r, None, None)) => {
        val vNow = compChange(distance, p, time, t)
        TickRecord(distance, time, throttle, Some(vNow), None)
      }
      case Some(TickRecord(p, t, r, Some(v), _)) => {
        val vNow = compChange(distance, p, time, t)
        val aNow = compChange(vNow, v, time, t)
        TickRecord(distance, time, throttle, Some(vNow), Some(aNow))
      }
    }
    this.enqueue(currentRecord)
  }
  def compChange(now: Double, last: Double, tNow: Long, tLast: Long) = {
    (now-last)/(tNow-tLast)
  }
  def estimates = if(isFull){ unknowns } else {
    try{
      val rvs = rvSum
      val avs = avSum
      val ars = arSum
      val vss = vSqSum
      val rss = rSqSum

      val engine = (rvs*avs - ars*vss)/(rvs*rvs - rss*vss)
      val drag = (engine*rss-ars)/rvs
      unknowns = Unknowns(engine, drag)
      unknowns
    } catch {
      case e:ArithmeticException => unknowns
    }
  }
  def getDouble(opt: Option[Double]):Double = opt.getOrElse(0.0)
  def sumOp(op:TickRecord => Double):Double = foldLeft(0.0)(
    (sum:Double, record:TickRecord) => sum + op(record)
  )
  def rvSum:Double = sumOp(rec => rec.r * getDouble(rec.v))
  def avSum:Double = sumOp(rec => getDouble(rec.a) * getDouble(rec.v))
  def arSum:Double = sumOp(rec => getDouble(rec.a) * rec.r)
  def vSqSum:Double = sumOp(rec => getDouble(rec.v) * getDouble(rec.v))
  def rSqSum:Double = sumOp(rec => rec.r * rec.r)
}

object Estimator {
  val records = new Records()
  def feed(distance: Double, time: Long, throttle: Double) =
    records.makeRecord(distance, time, throttle)
  def estimates = records.estimates

  def p = 0.02
  def f = 0.2*turbo
  def turbo {
    val useTurbo = false
    if(ChangingData.turboActive && useTurbo){
      ChangingData.turbo.head.turboFactor
    }else{ 1.0 }
  }
  def maxVelocity = f / p

  def distanceTo(targetVelocity: Double, currentVelocity: Double, throttle: Double) = {
    val v0 = targetVelocity
    val v = currentVelocity
    val r = throttle

    val p_2 = p * p
    val rfvp = r*f - v*p
    val rfv0p = r*f - v0*p

    (-r*f/p_2) * Math.log(Math.abs(rfvp / rfv0p)) + rfvp / p_2 - rfv0p / p_2
  }

  def optimalThrottle(v0: Double, v1: Double) = (v0, v1) match {
    case (_, v1) if v1 == Double.PositiveInfinity => 1.0
    case (v0, v1) if Math.abs(v0 - v1) <= 0.0001 => v0 * p / f
    case (v0, v1) if v0 > v1 => 0.0
    case (v0, v1) if v0 < v1 => 1
  }

  def velocityAfter(currentVelocity: Double, distance: Double, throttle: Double) = {
    val v0 = currentVelocity
    val l = distance
    val r = throttle

    if(r == 0) {
      v0 - p * l
    }
    else {
      val v0p = v0 * p
      val rf = r * f
      val v0plp_2rf = (v0p - l*p*p)/rf

      val lamberted = lambertW( (v0p/rf-1) * Math.exp(v0plp_2rf - 1) )

      (rf - Math.exp( v0plp_2rf - lamberted - 1 ) * (rf - v0p)) / p
    }
  }

  def lambertW(v: Double) = {
    import dr.evomodel.epidemiology.LambertW
    try {
      LambertW.branch0(v)
    }
    catch { case e: Exception =>
      // I dunno
      1.0
    }
  }

  def throttles(laneIndex: Int, laps: Int): List[(Double, Double, Double)] = {
    val track = StaticData.race.get.track
    val offset = track.lanes(laneIndex).distanceFromCenter
    val pieces = track.pieces

    val out = scala.collection.mutable.Stack.empty[(Double, Double, Double)]
    var nextVel = pieces.last.safeVelocity(offset)

    for(lap <- laps to 0 by -1; i <- pieces.size - 1 to 0 by -1) {
      val vi = pieces(i).safeVelocity(offset)
      val li = track.distance(i, laneIndex, lap)
      val pieceLength = pieces(i).distance(offset)

      if(vi > nextVel) {
        val r = optimalThrottle(vi, nextVel)
        val dist = distanceTo(vi, nextVel, r)

        if(dist < pieceLength) {
          out.push( (r, li + pieceLength, vi) )
          out.push( (optimalThrottle(vi, vi), li + pieceLength - dist, vi ) )
          nextVel = vi
        }
        else {
          val startVel = velocityAfter(nextVel, -pieceLength, r)
          out.push( (optimalThrottle(startVel, nextVel), li + pieceLength, startVel) )
          nextVel = startVel
        }
      }
      else {
        out.push( (optimalThrottle(vi, nextVel), li + pieceLength, vi) )
        nextVel = vi
      }
    }
    out.toList
  }
}
